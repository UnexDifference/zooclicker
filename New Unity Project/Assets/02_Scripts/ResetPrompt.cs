﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    public class ResetPrompt : MonoBehaviour
    {
        public GameObject PromptScreen;

        public void Prompt()
        {
            PromptScreen.SetActive(true);
        }

        public void Disprompt()
        {
            PromptScreen.SetActive(false);
        }
    }
}
