﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{
    [CreateAssetMenu(fileName = "Skill", menuName = "ScriptableObjects/Skills", order =1)]

    public class SO : ScriptableObject
    {
        public int addValue;
        public int worth;
    }
}
