﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LAPMTech
{

    public class QuitGame : MonoBehaviour
    {
        public void Quit()
        {
            Application.Quit();
        }
    }
}
