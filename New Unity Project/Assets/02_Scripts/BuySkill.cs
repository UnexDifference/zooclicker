﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LAPMTech;


namespace LAPMTech
{

    public class BuySkill : MonoBehaviour
    {

        public SO mySO;
        Clicker myClicker;
        int worth;
        int AddValue;
        float pets;
        public int passValue = 1;
        int LevelReached;



        private void Start()
        {
            worth = mySO.worth;
            pets = PlayerPrefs.GetFloat("Pets");
            AddValue = mySO.addValue;
            LevelReached = PlayerPrefs.GetInt("levelReached");
            
        }

        public void Skillbuy()
        {
            if (pets > worth)
            {

                pets -= worth;
                PlayerPrefs.SetFloat("Pets", pets);
                passValue += AddValue;
                PlayerPrefs.SetFloat("pass", passValue);
                Debug.Log(passValue);

               
                

            }
            else
            {
                Debug.Log("you do not have enough petpoints");
            }
        }
    }
}
