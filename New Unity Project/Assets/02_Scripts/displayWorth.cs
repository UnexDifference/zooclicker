﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace LAPMTech
{
    public class displayWorth : MonoBehaviour
    {
        [SerializeField]
        public float worth;

        [SerializeField]
        Button animal;

        [SerializeField]
        TextMeshProUGUI WorthDisplayer;

        void LateUpdate()
        {


            if (animal.interactable != true)
            {

                WorthDisplayer.text = ("Cost: " + worth);
            }
            else
            {
                WorthDisplayer.text = (" ");
            }
        }


    }
}
