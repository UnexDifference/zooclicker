﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LAPMTech
{
    public class BackButtonScript : MonoBehaviour
    {
        public void Back()
        {
            SceneManager.LoadScene("Gehegeauswahl");
        }
    }
}
