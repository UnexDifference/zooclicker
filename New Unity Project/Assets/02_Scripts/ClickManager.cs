﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;


namespace LAPMTech
{

    public class ClickManager : MonoBehaviour
    {
        public TextMeshProUGUI display;

        float pets;
        int worth;
        public Button[] levelButtons;
        public Button[] buySceneButtons;

        private void Start()
        {
            
        }


        void Update()
        {
            int levelISbuyable = PlayerPrefs.GetInt("levelISbuyable");
            int levelISenabled = PlayerPrefs.GetInt("levelReached");

            for (int i = 0; i < levelButtons.Length; i++)
            {
                if (i == 0)
                {
                    levelButtons[i].interactable = true;

                }
                else if (i + 1 > levelISenabled)
                {
                    levelButtons[i].interactable = false;
                }
                

            }

            


            for (int i = 0; i < buySceneButtons.Length; i++)
            {
                if(levelISbuyable ==1)
                {
                    buySceneButtons[i].interactable = true;
                }

                if (i + 1 > levelISbuyable)
                {
                    buySceneButtons[i].interactable = false;
                }
            }


            if (pets >= 250f)
            {
                PlayerPrefs.SetInt("levelISbuyable", 2);
            }

            if (PlayerPrefs.GetInt("levelReached") == 2)
            {
                PlayerPrefs.SetInt("levelISbuyable", 3);
            }

            if (PlayerPrefs.GetInt("levelReached")== 3)
            {
                PlayerPrefs.SetInt("levelISbuyable", 4);
            }

            if (PlayerPrefs.GetInt("levelReached") == 4)
            {
                PlayerPrefs.SetInt("levelISbuyable", 5);
            }

            if (PlayerPrefs.GetInt("levelReached") == 5)
            {
                PlayerPrefs.SetInt("levelISbuyable", 6);
            }
            if (PlayerPrefs.GetInt("levelReached") == 6)
            {
                PlayerPrefs.SetInt("levelISbuyable", 7);
            }
            if (PlayerPrefs.GetInt("levelReached") == 7)
            {
                PlayerPrefs.SetInt("levelISbuyable", 8);
            }
            if (PlayerPrefs.GetInt("levelReached") == 8)
            {
                PlayerPrefs.SetInt("levelISbuyable", 9);
            }
            if (PlayerPrefs.GetInt("levelReached") == 9)
            {
                PlayerPrefs.SetInt("levelISbuyable", 10);
            }



            display.text = ("You have " + PlayerPrefs.GetFloat("Pets") + " Petpoints");
            pets = PlayerPrefs.GetFloat("Pets");
            
        }

       




    }
}