﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace LAPMTech
{

    public class ResetPLayerPrefs : MonoBehaviour
    {
        public GameObject PromptScreen;
        

        public void DeletePlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            PromptScreen.SetActive(false);
            SceneManager.LoadScene("Gehegeauswahl");

        }
    }
}
