﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace LAPMTech
{
    public class BuyLevel : MonoBehaviour
    {
        displayWorth DisplayWorth;
        public Button Levelbutton;
        float pets;
        public float worth;


        private void Start()
        {
            pets = PlayerPrefs.GetFloat("Pets");

        }

        private void Update()
        {
            int LevelISReached = PlayerPrefs.GetInt("levelReached");
        }

        public void Buy()
        {
            int LevelISReached = PlayerPrefs.GetInt("levelReached");

            if (pets > worth)
            {


                pets -= worth;
                PlayerPrefs.SetFloat("Pets", pets);
                LevelISReached += 1;
                PlayerPrefs.SetInt("levelReached", LevelISReached);



                if (PlayerPrefs.GetInt("levelReached") == 1)
                {
                    PlayerPrefs.SetInt("levelReached", 2);
                }
                Debug.Log(LevelISReached);
                Levelbutton.interactable = false;
                DontDestroyOnLoad(Levelbutton);
                SceneManager.LoadScene("Gehegeauswahl");

            }
            else
            {
                Debug.Log("you do not have enough petpoints");
            }
        }


    }
}
